//
// Created by aard on 20.10.2021.
//
// @todo Invoke virtual methods.
// @todo Think more about full list of methods.
enum e_CardValues {
  two,
  three,
  queen,
  king,
  ace
};
class Card {
 protected:
  e_CardValues value;
  // @todo Suit doesn't matter in simple version.
  //e_CardSuit suit;
 public:
  Card(e_CardValues value): value(value) {}
  virtual int getScore(int handScore) {return 0;}
};
class Deck {
 protected:
  const int CARDS_IN_DECK = 52;
  Card** cards;
  // More easy (and potential safe) have a friend class than public method to set card order.
  friend class Shoe;
 public:
  // Add cards to deck.
  Deck() {
    cards = new Card*[CARDS_IN_DECK];
    for (int i = 0; i < CARDS_IN_DECK; i++) {
      // @todo Init cards (not now).
    }
    resetDeck();
  }
  ~Deck() {
    for (int i = 0; i < CARDS_IN_DECK; i++) {
      delete cards[i];
    }
    delete cards;
  }
  virtual void resetDeck() {}
};

// To store and shuffle some decks.
class Shoe {
 private:
  Deck** decks;
  int decks_count;
  virtual void Shuffle() {};
 public:
  explicit Shoe(int decks_count) {
    this->decks_count = decks_count;
    decks = new Deck*[decks_count];
    for (int i = 0; i < decks_count; i++) {
      decks[i] = new Deck();
    }
  }
  ~Shoe() {
    for (int i = 0; i < decks_count; i++) {
      delete decks[i];
    }
    delete decks;
  }
  virtual Card* GetCard() {return new Card(*new e_CardValues[0]);}
};

class PlayerBase {
 protected:
  int cards_count;
  Card** hand;
 public:
  PlayerBase() {
    cards_count = 0;
    hand = nullptr;
  }
  ~PlayerBase() {
    for (int i = 0; i < cards_count; i++) {
      delete hand[i];
    }
    delete hand;
  }
  virtual void hitAction() {}
  virtual void standAction() {}
};
// @todo Nothing to implement? Could be useful to count win sum in the future.
class Dealer : public PlayerBase {

};
class Player : public PlayerBase {
 protected:
  // Virtual player to make split action.
  Player* split = nullptr;
 public:
  virtual void splitAction() {}
};

class GameEngine {
 private:
  int players_count;
  PlayerBase** players;
  Shoe* shoe;
 public:
  GameEngine(int players_count, int decks_count): players_count(players_count) {
    players = new PlayerBase*[players_count];
    players[0] = new Dealer();
    for (int i = 1; i < players_count; i++) {
      players[i] = new Player();
    }
    shoe = new Shoe(decks_count);
  }
  ~GameEngine() {
    for (int i = 0; i < players_count; i++) {
      delete players[i];
    }
    delete players;
    delete shoe;
  }
};
